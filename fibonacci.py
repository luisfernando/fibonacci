# -*- coding: utf-8 -*-
"""
Fibonacci Utilities
"""
import unittest

class Fibonacci(object):
    """
    All kind of utilities to calculate Fibonacci numbers
    """
    def __init__(self):
        self.storage = {1: 1, 2: 1}

    def get(self, n):
        """ Returns the n number of the fibonacci series
        Args:
            n: The position n of the Fibonacci series.
        Returns:
            The number n in the Fibonacci series
        """
        if not self.storage.get(n):
            self.calculate(n)
        return self.storage.get(n)

    def calculate(self, n):
        """
        Sets into memory the n number of the Fibonacci series
        """
        if not self.storage.get(n):
            self.storage[n] = self.get(n-1) + self.get(n-2)

    def generator(self, n):
        """
        Returns a generator of fibonacci numbers
        :rtype: tuple
        """
        if not self.storage.get(n):
            self.calculate(n)
        for k in self.storage:
            yield (k, self.storage[k])

    def print_(self, n):
        """
        Prints the fibonacci numbers
        """
        for k, v in self.generator(n):
            print "%s,   %s" % (k, v)


class FibonacciTest(unittest.TestCase):
    def setUp(self):
        self.root = Fibonacci()

    def test_get(self):
        self.assertEqual(self.root.get(3), 2)

    def test_calculate(self):
        self.assertIsNone(self.root.calculate(10))
